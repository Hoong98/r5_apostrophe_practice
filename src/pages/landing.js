import { useState, useContext, useRef } from 'react';
import { Link } from "react-router-dom";
import PreLoaderContext from '../context/preload_context';
import LottieAnimation from '../components/lottie';


import { Video, Audio } from '../components/components'

import { lotties } from '../config/lotties_config';


export default function Landing() {
  const [show, setShow] = useState(false);
  const [start, setStart] = useState(true);
  const preContext = useContext(PreLoaderContext);
  const audio = useRef();

  function play() {
    audio.current.play();
    preContext.play();
    setStart(false)
  }
  return <>
    <Video src={preContext.videos.back_video} style={style.full_video} preLoad={true} className="absolute" delay={0} mute={true} autoPlay={true} loop={true} />
    <Audio src={preContext.audios.title_audio} preLoad={true} onEnded={() => setShow(true)} ref={audio} />
    <img src={preContext.images.landing_title} alt="" className="absolute" style={style.landing_img} />
    <LottieAnimation data={lotties.intro} loop={true} style={style.lottie} />
    {start && <img src={preContext.images.start_button} alt="" onClick={play} className="center pointer" style={style.start_button} />}
    {show && <Link to='/login'><img src={preContext.images.next_button} className="center animated fadeIn bottom-right" style={style.next_button} alt="" /></Link>}
  </>
}



const style = {
  landing_img: {
    position: 'absolute',
    width: '62%',
    top: '-20%',
    left: '8%',
    right: '0%',
    bottom: '0%',
    textAlign: 'center',
    margin: 'auto',
  },
  next_button: {
    width: '15%'
  },
  full_video: {
    width: '100%',
    left: 0,
    textAlign: 'center',
  },
  start_button: {
    width: '17%',
    bottom: '10%',
    zIndex: 0,
    left: '5%'
  },
  speaker: {
    width: '3.5%',
    left: '63%',
    top: '30%'
  },
  lottie: {
    top: '0%',
    width: '100%',
    left: '0%'
  },
}
