import  { useState, useRef } from 'react';
import { HashRouter as Router, Switch, Route } from "react-router-dom";
import menu from '../config/menu';
import LottieAnimation    from '../components/lottie';
import {lotties} from '../config/lotties_config';

//cache preload media
import PreLoader from '../components/preload_media.js';
import PreLoaderContext from '../context/preload_context';
import SessionContext   from '../context/session_context';

import {Audio} from '../components/components';


//css
import '../assets/css/style.css';


export default function Container() {
  const [loading,setLoading]   = useState(true);
  const [images,setImages]     = useState();
  const [videos,setVideos]     = useState();
  const [audios,setAudios]     = useState();
  const [session,setSession]   = useState();
  const audio                  = useRef();

  function play() {
     audio.current.play();
  }
  function stop() {
    audio.current.pause();
  }
  return  <>{loading ? <div className="full_center"><LottieAnimation data={lotties.loading} loop={true} style={style.lottie}/><PreLoader loading={setLoading} {...{setImages,setVideos,setAudios}}/></div> :
            <PreLoaderContext.Provider value={{images,videos,audios,play,stop}} >
              <SessionContext.Provider value={{session,setSession}} >
                <div className="container">
                <Audio src={audios.back_audio_loop} preLoad={true}  ref={audio} loop={true} />
                <Router>
                  <Switch>
                  {menu.map(a=> <Route key={a.path} exact={true} path={(a.route_path ? a.route_path:a.path)} render={a.component}/>)}
                  </Switch>
                </Router>
                </div>
              </SessionContext.Provider>
              </PreLoaderContext.Provider>
        }</>
}


const style = {
   lottie:{
     top:'0%',
     width:'80%',
     margin:'auto',
     display:'block',
     left:'0%',
     right:'0%'
   },
}
