import { useState, useContext, useRef } from 'react';
import { useHistory } from "react-router-dom";
import PreLoaderContext from '../context/preload_context';
import SessionContext from '../context/session_context';
import LottieAnimation from '../components/lottie';
import moment from 'moment';


import { Video, Audio } from '../components/components'

import { lotties } from '../config/lotties_config';


export default function Landing() {
  const [show, setShow] = useState(false);
  const history         = useHistory();
  const name_val        = useRef();
  const class_val       = useRef();
  const preContext      = useContext(PreLoaderContext);
  const sessionContext  = useContext(SessionContext);

  function setSession() {
    if (name_val.current.value.trim() && class_val.current.value.trim()) {
      sessionContext.setSession({
        name: name_val.current.value,
        class: class_val.current.value,
        time: Date.now()
      })
      history.push('/story')
    }
  }

  return <>
    <Video src={preContext.videos.back_video} style={style.full_video} preLoad={true} className="absolute" delay={0} mute={true} autoPlay={true} loop={true} />
    <div className="absolute" style={style.landing_img}>
      <img src={preContext.images.login_box} alt="" className="absolute full-width" />
      <input ref={name_val} className="absolute center input coco" style={{ top: '13%' }} />
      <input ref={class_val} className="absolute center input coco" style={{ top: '38%', fontWeight: 'bold' }} />
      <span className="absolute center input coco" style={style.date}>{moment().format('DD-MMM-YYYY')}</span>
    </div>
    <Audio src={preContext.audios.login_audio} preLoad={true} autoPlay={true} onEnded={() => setShow(true)} />
    <LottieAnimation data={lotties.intro} loop={true} style={style.lottie} />
    {show && <img src={preContext.images.next_button} onClick={setSession} className="center pointer animated fadeIn bottom-right" style={style.next_button} alt=""/>}

  </>
}



const style = {
  landing_img: {
    position: 'absolute',
    width: '43%',
    top: '-11%',
    left: '0%',
    right: '-15%',
    bottom: '0%',
    textAlign: 'center',
    margin: 'auto',
    height: '30%'
  },
  next_button: {
    width: '15%'
  },
  date: {
    top: '64%',
    paddingLeft: 0,
    fontSize: '1.7vw',
    fontWeight: 'bold'
  },
  full_video: {
    width: '100%',
    left: 0,
    textAlign: 'center',
  },
  start_button: {
    width: '17%',
    bottom: '10%',
    zIndex: 0,
    left: '5%'
  },
  speaker: {
    width: '3.5%',
    left: '63%',
    top: '30%'
  },
  lottie: {
    top: '0%',
    width: '100%',
    left: '0%'
  },
}
