import { useState, useContext } from 'react';
import { Link } from "react-router-dom";
import PreLoaderContext from '../context/preload_context';



//component
import { Video } from '../components/components'

export default function Story() {
  const [show, setShow] = useState(false);
  const preContext = useContext(PreLoaderContext)

  return <>
    <Video src={preContext.videos.story_back} style={style.full_video} preLoad={true} className="absolute" delay={0} onEnded={() => setShow(true)} autoPlay={true} loop={false} />
    {!show && <Link to='/game'><img src={preContext.images.skip_button} className="center animated fadeIn bottom-right" alt="" style={style.next_button} /></Link>}
    {show && <Link to='/game'><img src={preContext.images.next_button} className="center animated fadeIn bottom-right" alt="" style={style.next_button} /></Link>}
  </>
}



const style = {
  full_video: {
    width: '100%',
    left: 0,
    textAlign: 'center',
  },
  center_button: {
    width: '50%',
    bottom: '5%',
    zIndex: -1
  },
  next_button: {
    width: '15%'
  }
}
