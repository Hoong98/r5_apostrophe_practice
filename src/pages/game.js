import { useState, useEffect, useContext, useRef } from 'react';
import { useHistory } from "react-router-dom";
import PreLoaderContext from '../context/preload_context';

import { Video } from '../components/components'



export default function Game() {
  const history = useHistory();
  const [show, setShow] = useState(false);
  const preContext = useContext(PreLoaderContext);
  const [indexArr, setIndexArr] = useState(0);
  const [questionArr, setquestionArr] = useState(null);
  const [result, setResult] = useState([]);
  const limit = 8;
  const timing = [
    { correct: [0.5, 9], wrong: [31.5, 36] },
    { correct: [10.5, 19], wrong: [37.5, 43] },
    { correct: [20.5, 29], wrong: [44.5, 51] }
  ];
  const video_list = [
    {
      nrml: preContext.images.servant_nrml,
      black: preContext.images.servant_black,
      video: preContext.videos.servent_video,
    },
    {
      nrml: preContext.images.baker_nrml,
      black: preContext.images.baker_black,
      video: preContext.videos.baker_video,
    },
    {
      nrml: preContext.images.bird_nrml,
      black: preContext.images.bird_black,
      video: preContext.videos.bird_video,
    },
    {
      nrml: preContext.images.kid_nrml,
      black: preContext.images.kid_black,
      video: preContext.videos.kid_video,
    },
    {
      nrml: preContext.images.man_nrml,
      black: preContext.images.man_black,
      video: preContext.videos.man_video,
    },
    {
      nrml: preContext.images.sheep_nrml,
      black: preContext.images.sheep_black,
      video: preContext.videos.sheep_video,
    },
    {
      nrml: preContext.images.women_nrml,
      black: preContext.images.women_black,
      video: preContext.videos.women_video,
    },
    {
      nrml: preContext.images.king_nrml,
      black: preContext.images.king_black,
      video: preContext.videos.king_video,
    },
  ];
  const arr = [
    {
      question: <>Do you know any of <span className="underline">the servants of the King?</span></>,
      answer: <>Do you know any of <span className="coloring">the King’s servants?</span></>,
      ans: [
        { text: "the King's servants", correct: true },
        { text: "the Kings' servants" },
        { text: "the King servants" },
      ]
    },
    {
      question: <>I will meet you at <span className="underline">the gate of our school</span>.</>,
      answer: <>I will meet you at <span className="coloring">the school gate</span>.</>,
      ans: [
        { text: "the school gate", correct: true },
        { text: "the schools’ gate" },
        { text: "the school’s gate" },
      ]
    },
    {
      question: <>The coach paid for <span className="underline"> the shoes of the runners</span>.</>,
      answer: <>The coach paid for <span className="coloring"> the runners’ shoes</span>.</>,
      ans: [
        { text: "the runners’ shoes", correct: true },
        { text: "the shoes’ runners" },
        { text: "the runner’s shoes" },
      ]
    },
    {
      question: <><span className="underline">The neck of the giraffe </span> is long and flexible.</>,
      answer: <><span className="coloring">The giraffe’s neck </span> is long and flexible. </>,
      ans: [
        { text: "The giraffe’s neck ", correct: true },
        { text: "The giraffes’ neck" },
        { text: "The giraffe’ neck" },
      ]
    },
    {
      question: <><span className="underline">The eyes of James </span>  were swollen this morning.</>,
      answer: <><span className="coloring">James’s eyes</span>  were swollen this morning.</>,
      ans: [
        { text: "James’s eyes", correct: true },
        { text: "Jame’s eyes" },
        { text: "James eye’s" },
      ]
    },
    {
      question: <><span className="underline">The jackets of the gentlemen </span> look very expensive.</>,
      answer: <><span className="coloring">The gentlemen’s jackets </span> look very expensive.</>,
      ans: [
        { text: "The gentlemen’s jackets", correct: true },
        { text: "The gentlemens’ jackets" },
        { text: "The gentlemen jackets" },
      ]
    },
    {
      question: <><span className="underline">The handles of the knives </span> were shaped differently.</>,
      answer: <><span className="coloring">The handles of the knives </span> were shaped differently.</>,
      ans: [
        { text: "The handles of the knives", correct: true },
        { text: "The knives’ handles" },
        { text: "The knives’s handles" },
      ],
    },
    {
      question: <>Open  <span className="underline">  the windows of the room!</span> </>,
      answer: <>Open <span className="coloring">  the room windows!</span></>,
      ans: [
        { text: "the room windows", correct: true },
        { text: "the rooms’ windows" },
        { text: "the room’s windows" },
      ]
    },
    {
      question: <>There were many <span className="underline"> caps belonging to policemen </span> on the
display shelf. </>,
      answer: <>There were many <span className="coloring"> policemen’s caps</span> on the
display shelf.</>,
      ans: [
        { text: "policemen’s caps", correct: true },
        { text: "policemen caps" },
        { text: "policemens’ caps" },
      ]
    },
    {
      question: <>The genie turned <span className="underline"> the tails of two
foxes  </span>  into a lovely fur coat. </>,
      answer: <>The genie turned <span className="coloring"> two foxes’ tails</span>  into a lovely fur coat.</>,
      ans: [
        { text: "two foxes’ tails", correct: true },
        { text: "two fox’s tails" },
        { text: "two foxes tails" },
      ]
    },
    {
      question: <><span className="underline">The paw of the bear </span> was hurt.</>,
      answer: <><span className="coloring">The bear’s paw </span> was hurt.</>,
      ans: [
        { text: "The bear’s paw", correct: true },
        { text: "The bear paw" },
        { text: "The bears’ paw" },
      ],

    },
    {
      question: <>Who can correct <span className="underline"> the mistake of
Alice? </span></>,
      answer: <>Who can correct <span className="coloring"> Alice’s mistake?</span></>,
      ans: [
        { text: "Alice’s mistake", correct: true },
        { text: "Alice’ mistake" },
        { text: "Alices’ mistake" },
      ]
    },
    {
      question: <><span className="underline">The cots used by the babies  </span> are placed neatly in rows.</>,
      answer: <><span className="coloring">The babies’ cots</span> are placed neatly in rows.</>,
      ans: [
        { text: "The babies’ cots", correct: true },
        { text: "The babie’s cots" },
        { text: "The babies cots’" },
      ],
    },
    {
      question: <><span className="underline">The hands of the children </span> are dirty.</>,
      answer: <><span className="coloring">The children’s hands </span> are dirty.</>,
      ans: [
        { text: "The children’s hands", correct: true },
        { text: "The children hands" },
        { text: "The childrens’ hands" },
      ],
    },
    {
      question: <><span className="underline">The handphone belonging to Shanti  </span> was found at the canteen.</>,
      answer: <><span className="coloring">Shanti’s handphone </span> was found at the canteen.</>,
      ans: [
        { text: "Shanti’s handphone", correct: true },
        { text: "Shanti handphone" },
        { text: "Shantis’ handphone" },
      ],
    }
  ];

  useEffect(() => {
    setquestionArr(
      suffle(arr
        .map((a, b) => ({
          ...a,
          ans: suffle(keySetter(a.ans)),
          key: b,
          audio: preContext.audios[`q${b + 1}_audio`]
        }))
      ).slice(0, limit)
        .map((a, b) => ({ ...a, ...video_list[b] }))
    )
  }, [])

  function setSelect(a) {
    setResult([...result, a]);
    setShow(true);
  }

  function setNext() {
    if (indexArr + 1 !== questionArr.length) {
      setIndexArr(indexArr + 1);
    } else {
      history.push({ pathname: '/report', state: { result, questionArr } })
    }
  }

  return <>
    <img src={preContext.images.game_header} alt="" className="absolute" style={style.header} />
    {questionArr && <div style={style.footer} className="absolute">
      {questionArr.map((a, b) => {
        let finishedArr = result.find((i) => i.ques_key == a.key && i.correct == true);
        let finished = typeof finishedArr != 'undefined' ? finishedArr['correct'] : false;
        return <img key={b} style={{ left: (b * 7) + '%' }} src={finished ? a.nrml : a.black} className="absolute img_bottom" alt="" />;
      })}
    </div>}
    {questionArr && <Question key={indexArr} index={indexArr} data={questionArr[indexArr]} timing={timing} showNext={setShow} setSelect={setSelect} />}
    {show && <img src={preContext.images.next_button} onClick={setNext} className="center pointer animated fadeIn bottom-right" style={style.next_button} alt="" />}
  </>
}

const keySetter = (a) => a.map((a, b) => ({ ...a, key: b }))
function suffle(arr) {
  return arr.sort((a, b) => 0.5 - Math.random());
}

function Question(props) {
  const preContext = useContext(PreLoaderContext);
  const data = props.data;
  const [disabled, setDisabled] = useState(false);
  const [active, setActive] = useState(null);
  const [attempt, setAttempt] = useState();
  const [tryShow, setTry] = useState(false);

  const video = useRef();
  let interval = null;
  let result = { ans_key: '', ques_key: '' };

  function select(a) {
    if (!disabled) {
      setDisabled(true)
      setTimeout(function () {
        setActive(a.ans_key);
      }, 5000);
      let times = a.timing[a.correct ? 'correct' : 'wrong'];

      result = {
        ans_key: a.ans_key,
        ques_key: a.ques_key,
        correct: a.correct
      }
      video.current.currentTime = times[0];
      video.current.play();
      interval = setInterval(function () {
        if (video.current.currentTime >= times[1]) {
          // setDisabled(false)
          video.current.pause();
          if (!a.correct && attempt == 2) {
            setTry(true);
            setAttempt(attempt - 1)
          } else {
            if (!a.correct) {
              setAttempt(attempt - 1);
              correctPlay(() => props.setSelect(result))
            } else {
              props.setSelect(result);
            }
          }
          clearInterval(interval)
        }
      }, 1000);

    }
  }

  function correctPlay(callback) {
    let cur_stamp_ind = data.ans.findIndex(a => a.correct == true);
    let cur_time = props.timing[cur_stamp_ind]['correct'];
    video.current.currentTime = cur_time[0];
    video.current.play();
    let inter = setInterval(function () {
      if (video.current.currentTime >= cur_time[1]) {
        video.current.pause();
        callback()
        clearInterval(inter)
      }
    }, 1000);
  }

  useEffect(() => {
    props.showNext(false)
    setDisabled(false)
    setActive(null);
    setAttempt(2)
    return () => {
      clearInterval(interval)
    }
  }, [])

  function resetAll() {
    video.current.currentTime = 0;
    setDisabled(false)
    setActive(null);
    setTry(false);
  }

  return <>
    <Video src={data.video} onLoad={(e) => e.currentTime = 0} ref={video} style={style.full_video} preLoad={true} className="absolute" autoPlay={false} delay={0} />
    {tryShow && <img src={preContext.images.try_again} className="center pointer animated fadeIn bottom-right" style={style.try_button} alt="" onClick={resetAll} />}
    <span className="absolute coco" style={style.mark}>{attempt}/2</span>
    <div className="absolute coco" style={style.header_title} ><span style={{ fontSize: '2vw', marginRight: '1%' }}>{props.index + 1}/8 </span> {data.question}</div>
    {data.ans.map((a, b) => {
      let color = active == a.key ? (a.correct ? '#21ff21' : '#ff0b0b') : '#fff';
      return <span key={a.text} onClick={() => select({
        ques_key: data.key,
        correct: (a.correct ? a.correct : false),
        ans_key: a.key,
        timing: props.timing[b]
      })} className={`ans_box coco pointer position_${b}`} style={{ color }} >{a.text}</span>
    })}
  </>
}




const style = {
  landing_img: {
    position: 'absolute',
    width: '62%',
    top: '-20%',
    left: '8%',
    right: '0%',
    bottom: '0%',
    textAlign: 'center',
    margin: 'auto',
  },
  header: {
    left: '0%',
    right: '0%',
    width: '70%',
    margin: 'auto',
    top: '0%',
    zIndex: 1
  },
  header_title: {
    left: '0%',
    right: '0%',
    margin: 'auto',
    textAlign: 'center',
    fontSize: '1.3vw',
    top: '6%',
    width: '70%',
    color: '#fff',
    zIndex: 2
  },
  footer: {
    left: '21%',
    right: '0%',
    width: '100%',
    margin: 'auto',
    bottom: '0%',
    height: '20%',
    top: 'initial',
    zIndex: 1
  },
  full_video: {
    width: '100%',
    left: 0,
    textAlign: 'center',
  },
  start_button: {
    width: '17%',
    bottom: '10%',
    zIndex: 0,
    left: '5%'
  },
  speaker: {
    width: '3.5%',
    left: '63%',
    top: '30%'
  },
  lottie: {
    top: '0%',
    width: '100%',
    left: '0%'
  },
  next_button: {
    width: '13%',
    zIndex: 3,
    bottom: '2%'
  },
  try_button: {
    width: '13%',
    zIndex: 3,
    bottom: '2%',
    height: '10%'
  },
  mark: {
    left: '10%',
    top: 'initial',
    bottom: '3%',
    width: '10%',
    fontSize: '2.8vw',
    color: '#fff',
    zIndex: '2',
    fontWeight: '500'
  },
}
