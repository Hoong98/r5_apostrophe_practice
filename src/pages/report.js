import { useState, useEffect, useContext, useRef } from 'react';
import PreLoaderContext from '../context/preload_context';
import SessionContext from '../context/session_context';
import moment from 'moment';

import { Audio, Video } from '../components/components'


export default function Report(props) {
  const preContext = useContext(PreLoaderContext);
  const sessionContext = useContext(SessionContext);
  const [data, setData] = useState(null);
  const [correct, setCorrect] = useState(0);
  const [play, setPlay] = useState(false);
  const audioRef = useRef();
  const printRef = useRef();

  useEffect(() => {
    preContext.stop();
    if (props.location.state) {
      let pre_result = props.location.state.result;
      let question = props.location.state.questionArr;
      let resultArr = [];
      let crt = 0;
      pre_result.forEach((a) => {
        let ind = question.findIndex(i => i.key == a.ques_key);
        let arr = question[ind];
        let ch_ind = arr['ans'].findIndex(i => i.key == a.ans_key);
        arr['ans'][ch_ind]['selected'] = true;
        if (arr['ans'][ch_ind]['correct']) {
          crt += 1;
        }
        resultArr.push(arr)
      })
      setCorrect(crt);
      setData(resultArr);
    }
  }, [])

  function playAudio(a) {
    audioRef.current.src = a;
    audioRef.current.load();
    audioRef.current.play();
    setPlay(true)
  }
  return <>
    {data && <Audio src={preContext.audios.report_before} preLoad={true} delay={0} autoPlay={true} />}
    <Audio ref={audioRef} delay={0} onEnded={() => setPlay(false)} />
    <Video src={preContext.videos.back_video} style={style.full_video} preLoad={true} className="absolute" delay={0} mute={true} autoPlay={true} loop={true} />
    <div ref={printRef} className="absolute" style={{ ...style.landing_img, height: '100%' }}>
      <img src={preContext.images.print_button} onClick={() => window.print()} className="absolute pointer prevent_print" alt="" style={style.print_button} />
      {sessionContext.session && <><span className="absolute coco" style={{ ...style.date, left: '29.3%', top: '11.4%' }}>{sessionContext.session.name}</span>
        <span className="absolute coco" style={{ ...style.date, left: '29.3%', top: '16.4%' }}>{sessionContext.session.class}</span>
        <span className="absolute coco" style={style.date}>{moment(sessionContext.session.time).format('DD-MMM-YYYY')}</span></>}
      {data && <span className="absolute" style={style.mark}>{correct}/{data.length}</span>}
      <img src={preContext.images.resul_box} alt="" className="absolute" style={style.landing_img} />
      <img src={preContext.images.report_table} alt="" className="absolute" style={style.landing_img} />
      <ul className="absolute report_table">
        {data && data.map((a, b) => {
          if (a) {
            let ans_text_arr = a.ans.find(i => i.selected === true);
            let ans_text = ans_text_arr.text ? ans_text_arr.text : '';
            return <li key={'loop_' + b.toString()} style={{ top: (b * 12.5) + '%' }}>
              <AudioPlay onEnd={play} playAudio={() => { playAudio(a.audio); }} />
              <span className="text_box coco">{a.question}</span>
              <span className="text_box coco text_box_second">{a.answer}</span>
              <span className="text_box_answer coco absolute " style={{ color: (ans_text_arr.correct ? '#21ff21' : '#ff0b0b') }}>{ans_text}</span>
            </li>
          }
        })}
      </ul>
    </div>
  </>
}

function AudioPlay(props) {
  const preContext = useContext(PreLoaderContext);
  const [play, setPlay] = useState(false);
  useEffect(() => {
    if (!props.onEnd) {
      setPlay(false)
    }
  }, [props.onEnd])

  useEffect(() => {
    return () => {
      setPlay(false)
    }
  }, [])

  function playStart() {
    props.playAudio();
    setPlay(true);
  }
  return <img src={play ? preContext.images.audio_play : preContext.images.audio_button} onClick={playStart} className="speaker pointer absolute" alt=""/>;
}



const style = {
  landing_img: {
    position: 'absolute',
    width: '100%',
    top: '0%',
    left: '0%'
  },
  full_video: {
    width: '100%',
    left: 0,
    textAlign: 'center',
  },
  pdf_button: {
    left: '67%',
    top: '10.5%',
    width: '4%',
    zIndex: '2',
  },
  date: {
    left: '53.3%',
    top: '11%',
    width: '10%',
    fontSize: '1.2vw',
    color: '#fff',
    zIndex: '2',
    fontWeight: '500'
  },
  mark: {
    left: '54.5%',
    top: '14.2%',
    width: '10%',
    fontSize: '2.8vw',
    color: '#fff',
    zIndex: '2',
    fontWeight: '500'
  },
  print_button: {
    left: '70%',
    top: '10.5%',
    width: '4%',
    zIndex: '2',
  }

}
