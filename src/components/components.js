import { useState, useEffect, forwardRef } from 'react';


export const Audio = forwardRef((props, ref) => {
  const [show, setShow] = useState(false);

  useEffect(() => {
    setTimeout(() => {
      setShow(true)
    }, props.delay);
  }, [props.delay])

  return show && <audio preload={"true"} style={props.style} className={props.className} src={props.src} ref={ref} autoPlay={props.autoPlay} loop={props.loop} onEnded={props.onEnded} onPlay={props.onPlay} type="audio/mpeg"></audio>
})

function rand() {
  return 'key_rest' + Math.round(Math.random() * 10000);
}

export const Video = forwardRef((props, ref) => {
  const [show, setShow] = useState(false);
  useEffect(() => {
    setTimeout(() => {
      setShow(true)
    }, props.delay);
  }, [props.delay])

  return show && <video muted={props.d} preload={"true"} onPlaying={props.onPlaying} style={props.style} controls={false} className={props.className} src={props.src} ref={ref} autoPlay={props.autoPlay} loop={props.loop} onEnded={props.onEnded} onPlay={props.onPlay} onLoad={props.onLoad} type="audio/mpeg"></video>
})


