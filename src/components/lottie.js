import { useState } from 'react';
import Lottie from 'react-lottie';

export default function LottieAnimation(props) {
  const [isStopped, setStopped] = useState(false);
  const defaultOptions = {
    autoplay: true,
    loop: props.loop ? props.loop : false,
    animationData: props.data,
  }

  return <Lottie options={defaultOptions} isClickToPauseDisabled={true} style={{ ...style.lottie, ...props.style }} isStopped={isStopped} />
}


const style = {
  lottie: {
    width: '90%',
    overflow: 'hidden',
    margin: '0px auto',
    outline: 'none',
    position: 'absolute',
    top: '-5%',
    left: '5%'
  },
}
