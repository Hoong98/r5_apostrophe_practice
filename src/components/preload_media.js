import { useEffect } from 'react';
import { prefetch_file } from '../components/funs';
import { img, audio, video } from '../config/media';

function ImageLoad(url) {
  return new Promise(function (resolve, reject) {
    const loadImg = new Image();
    loadImg.src = url;
    loadImg.onload = () =>
      setTimeout(() => {
        const URL = window.URL || window.webkitURL;
        const blob_url = URL.createObjectURL(url);
        resolve(blob_url)
      }, 100)
    loadImg.onerror = err => reject(err)
  })
}

function mediaLoad(url) {
  return new Promise(function (resolve, reject) {
    prefetch_file(url, (res) => {
      resolve(res);
    }, (err) => {
      console.error(err);
      reject(err);
    })
  })
}

export default function PreLoader(props) {
  const img_arry = Object.entries(img);
  const audio_arry = Object.entries(audio);
  const video_arry = Object.entries(video);
  useEffect(() => {
    Promise.all([
      promiser(img_arry, props.setImages),
      promiser(audio_arry, props.setAudios),
      promiser(video_arry, props.setVideos)
    ]).then(() => {
      props.loading(false)
    }).catch(err => console.log("Failed to load images", err))
  }, [])
  return null;
}

function promiser(arr, callback) {
  return Promise.all(arr.map(async ([name, url]) => ({ [name]: await mediaLoad(url) }))).then((a) => callback(ArrayToObj(a)));
}
function ArrayToObj(arr) {
  return arr.reduce((acc, b) => (acc[Object.keys(b)[0]] = Object.values(b)[0], acc), {});
}
