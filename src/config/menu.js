
//pages
import Landing from '../pages/landing';
import Login from '../pages/login';
import Story from '../pages/story';
import Game from '../pages/game';
import Report from '../pages/report';


const menu = [
  {
    path: '/',
    preventMenu: true,
    component: () => <Landing />
  },
  {
    path: '/login',
    image: '',
    direction: 'left',
    top: 5,
    component: (props) => <Login {...props} />
  },
  {
    path: '/story',
    image: '',
    direction: 'left',
    top: 5,
    component: (props) => <Story {...props} />
  },
  {
    path: '/game',
    image: '',
    direction: 'left',
    top: 5,
    component: (props) => <Game {...props} />
  },
  {
    path: '/report',
    image: '',
    direction: 'left',
    top: 5,
    component: (props) => <Report {...props} />
  },
];


export default menu;
