
//<--------images-------->

//landing
import start_button from '../assets/img/landing/start_button.png';
import skip_button from '../assets/img/universal/skip_button.png';
import try_again from '../assets/img/universal/try_again.png';

//login
import login_box from '../assets/img/login/name-and-class-board.png'

//intro
import next_button from '../assets/img/universal/next_button.png';
import landing_title from '../assets/img/landing/title.png'

//game
import game_header from '../assets/img/game/02_question_header_box.png'
import game_footer from '../assets/img/game/footer_grey_box.png'

import servant_black from '../assets/img/game/Silhouette/01_servant-black.png';
import servant_nrml from '../assets/img/game/Silhouette/01_servant.png';
import baker_black from '../assets/img/game/Silhouette/02_baker-black.png';
import baker_nrml from '../assets/img/game/Silhouette/02_baker.png';
import bird_black from '../assets/img/game/Silhouette/03_bird-black.png';
import bird_nrml from '../assets/img/game/Silhouette/03_bird.png';
import kid_black from '../assets/img/game/Silhouette/04_kids-black.png';
import kid_nrml from '../assets/img/game/Silhouette/04_kids.png';
import man_black from '../assets/img/game/Silhouette/05_man-black.png';
import man_nrml from '../assets/img/game/Silhouette/05_man.png';
import sheep_black from '../assets/img/game/Silhouette/06_sheep-black.png';
import sheep_nrml from '../assets/img/game/Silhouette/06_sheep.png';
import women_black from '../assets/img/game/Silhouette/07_woman-black.png';
import women_nrml from '../assets/img/game/Silhouette/07_woman.png';
import king_black from '../assets/img/game/Silhouette/08_king-black.png';
import king_nrml from '../assets/img/game/Silhouette/08_king.png'

//report
import report_table from '../assets/img/report/table.png';
import print_button from '../assets/img/report/print-button.png';
import resul_box from '../assets/img/report/name-text-box.png';
import audio_button from '../assets/img/report/audio-button.png'
import pdf_button from '../assets/img/report/pdf-button.png'
import audio_play from '../assets/img/report/audio-button_playing.png'

//audio
//landing
import back_audio_loop from '../assets/img/universal/background_music.mp3'
import title_audio from '../assets/img/landing/title.mp3'

//login
import login_audio from '../assets/img/login/First_key_in.mp3'

//game
import report_before from '../assets/img/report/vo/07_Before you.mp3'
import q1_audio from '../assets/img/report/vo/Q1_Do you know.mp3'
import q2_audio from '../assets/img/report/vo/Q2_I will meet.mp3'
import q3_audio from '../assets/img/report/vo/Q3_The coach paid.mp3'
import q4_audio from '../assets/img/report/vo/Q4_The neck of.mp3'
import q5_audio from '../assets/img/report/vo/Q5_The eyes of.mp3'
import q6_audio from '../assets/img/report/vo/Q6_The jackets of.mp3'
import q7_audio from '../assets/img/report/vo/Q7_The handles of.mp3'
import q8_audio from '../assets/img/report/vo/Q8_Open the windows.mp3'
import q9_audio from '../assets/img/report/vo/Q9_There were many.mp3'
import q10_audio from '../assets/img/report/vo/Q10_The genie turned.mp3'
import q11_audio from '../assets/img/report/vo/Q11_The paw of.mp3'
import q12_audio from '../assets/img/report/vo/Q12_Who can correct.mp3'
import q13_audio from '../assets/img/report/vo/Q13_The cots used.mp3'
import q14_audio from '../assets/img/report/vo/Q14_The hands of.mp3'
import q15_audio from '../assets/img/report/vo/Q15_The handphone.mp3'

//report

//video
import back_video from '../assets/img/universal/back_video.mp4'

//story
import story_back from '../assets/img/story/22_Practice_witch_and_servant.mp4';

//game
import servent_video from '../assets/img/game/gameplay_animation_v2/servant.mp4';
import baker_video from '../assets/img/game/gameplay_animation_v2/baker.mp4';
import bird_video from '../assets/img/game/gameplay_animation_v2/bird.mp4';
import kid_video from '../assets/img/game/gameplay_animation_v2/kid.mp4';
import man_video from '../assets/img/game/gameplay_animation_v2/man.mp4';
import sheep_video from '../assets/img/game/gameplay_animation_v2/sheep.mp4';
import women_video from '../assets/img/game/gameplay_animation_v2/women.mp4';
import king_video from '../assets/img/game/gameplay_animation_v2/king.mp4'

const img = {
  start_button,
  next_button,
  landing_title,
  game_header,
  game_footer,
  report_table,
  print_button,
  resul_box,
  audio_button,
  pdf_button,
  login_box,
  skip_button,
  try_again,
  audio_play,
  servant_black,
  servant_nrml,
  baker_black,
  baker_nrml,
  bird_black,
  bird_nrml,
  kid_black,
  kid_nrml,
  man_black,
  man_nrml,
  sheep_black,
  sheep_nrml,
  women_black,
  women_nrml,
  king_black,
  king_nrml,
}


const audio = {
  report_before,
  q1_audio,
  q2_audio,
  q3_audio,
  q4_audio,
  q5_audio,
  q6_audio,
  q7_audio,
  q8_audio,
  q9_audio,
  q10_audio,
  q11_audio,
  q12_audio,
  q13_audio,
  q14_audio,
  q15_audio,
  back_audio_loop,
  title_audio,
  login_audio,
}

const video = {
  back_video,
  story_back,
  servent_video,
  baker_video,
  bird_video,
  kid_video,
  man_video,
  sheep_video,
  women_video,
  king_video
}


export { img, audio, video };
