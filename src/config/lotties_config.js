//<---lottie json-->
//using_more_linking

import intro    from '../assets/lotties/intro/intro.js';
import loading  from '../assets/lotties/loading.js';

const lotties = {
  intro,
  loading
}

export { lotties };
